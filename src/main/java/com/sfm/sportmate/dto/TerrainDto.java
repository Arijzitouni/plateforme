package com.sfm.sportmate.dto;

import com.sfm.sportmate.model.Terrain;

public class TerrainDto {

	public TerrainDto() {
		super();
	}

	public TerrainDto(Terrain Terrain) {
		super();
		this.id =Terrain.getId();
		
		this.adresse = Terrain.getAdresse();
		this.nom = Terrain.getNom();
		
		this.longitude = Terrain.getLongitude();
		this.latitude = Terrain.getLatitude();
		this.responsable = Terrain.getResponsable();
	}
	
	private long id;

	private String adresse;
	private String nom;

	private float longitude;

	private float latitude;
	private String responsable;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public Float getLongitude() {
		return (longitude);
	}

	public void setLongitude(Float Longitude) {
		this.longitude = Longitude;
	}
	public Float getLatitude() {
		return (latitude);
	}

	public void setLatitude(Float Latitude) {
		this.latitude = Latitude;
	}
	
	@Override
	public String toString() {
		return "UserDto [id=" + id + ", adresse=" + adresse + ", responsable=" + responsable + ",, nom="
				+ nom + " ]";
	}

}
	
	
	
	
	
	

