package com.sfm.sportmate.dto;

import com.sfm.sportmate.model.Role;
import com.sfm.sportmate.model.User;

public class UserDto {

	public UserDto() {
		super();
	}

	public UserDto(User user) {
		super();
		this.idUser = user.getIdUser();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.role = user.getRole();
		this.nom = user.getNom();
		this.prenom = user.getPrenom();
		this.email = user.getEmail();
		this.telephone = user.getTelephone();
		this.enable = user.isEnable();
	}
	/*public UserDto(String username, String password, String email) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;

	}*/

	private long idUser;

	private String username;
	private String password;

	private Role role;

	private String nom;
	private String prenom;
	private String email;
	private String telephone;
	private boolean enable;

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "UserDto [id=" + idUser + ", username=" + username + ", password=" + password + ", role=" + role + ", nom="
				+ nom + ", prenom=" + prenom + ", email=" + email + ", telephone=" + telephone + ", enable=" + enable
				+ "]";
	}

}
