package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.Feedback;

@Repository
public interface FeedbackDao extends CrudRepository<Feedback, Long> {

}
