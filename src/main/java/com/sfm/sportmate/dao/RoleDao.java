package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.Role;

@Repository
public interface RoleDao extends CrudRepository<Role, Long> {

}
