package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.Horaires;

@Repository
public interface HorairesDao extends CrudRepository<Horaires, Long> {

}