package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.Terrain;

@Repository
public interface TerrainDao extends CrudRepository<Terrain, Long> {

}
