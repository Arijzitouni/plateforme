package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.ItemsTerrain;

@Repository
public interface ItemsTerrainDAO extends CrudRepository<ItemsTerrain, Long> {

}