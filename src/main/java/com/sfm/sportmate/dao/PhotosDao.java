package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.Photos;

@Repository
public interface PhotosDao extends CrudRepository<Photos, Long> {

}
