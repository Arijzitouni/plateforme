package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.Discipline;

@Repository
public interface DisciplineDao extends CrudRepository<Discipline, Long> {

}