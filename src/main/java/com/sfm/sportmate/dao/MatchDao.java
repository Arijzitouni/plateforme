package com.sfm.sportmate.dao;

	import org.springframework.data.repository.CrudRepository;
	import org.springframework.stereotype.Repository;

	import com.sfm.sportmate.model.Match;

	@Repository
	public interface MatchDao extends CrudRepository<Match, Long> {

	}


