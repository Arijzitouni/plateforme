package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.Items;

@Repository
public interface ItemsDao extends CrudRepository<Items, Long> {

}