package com.sfm.sportmate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sfm.sportmate.model.DisciplineTerrain;

@Repository
public interface DisciplineTerrainDao extends CrudRepository<DisciplineTerrain, Long> {

}