package com.sfm.sportmate.config;

public class Constants {
    
    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 24 * 60 * 60; // 1 jour
    public static final String SIGNING_KEY = "SPORTMATE_SFM";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_ISSUER = "SFM";
    public static final String HEADER_STRING = "Authorization";
   
}
