package com.sfm.sportmate.service;

import java.util.List;

import com.sfm.sportmate.model.Participe;

public interface ParticipeService {
	
	Participe save(Participe Participe);
	
    List<Participe> findAll();
    
    void delete(long id);
    
    Participe findById(Long id);
   
}