package com.sfm.sportmate.service;

import java.util.List;

import com.sfm.sportmate.model.DisciplineTerrain;

public interface DisciplineTerrainService {

	DisciplineTerrain save(DisciplineTerrain DisciplineTerrain);
	
    List<DisciplineTerrain> findAll();
    
    void delete(long id);
    
    DisciplineTerrain findById(Long id);
    
}