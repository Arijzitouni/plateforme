package com.sfm.sportmate.service;
import java.util.List;

import com.sfm.sportmate.model.Feedback;
public interface FeedbackService {


	

	Feedback save(Feedback Feedback);
		
	    List<Feedback> findAll();
	    
	    void delete(long id);
	    
	    Feedback findById(Long id);
	    
	}
