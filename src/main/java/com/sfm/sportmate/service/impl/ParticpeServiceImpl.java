package com.sfm.sportmate.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.ParticipeDao;
import com.sfm.sportmate.model.Participe;
import com.sfm.sportmate.service.ParticipeService;


@Service(value = "Participe")
public class ParticpeServiceImpl implements ParticipeService{

	
	@Autowired
	private ParticipeDao ParticipeDao;

	@Override
	public Participe save(Participe Participe) {
		return ParticipeDao.save(Participe);
	}

	@Override
	public List<Participe> findAll() {
		List<Participe> list = new ArrayList<>();
		ParticipeDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		ParticipeDao.deleteById(id);
		
	}

	@Override
	public Participe findById(Long id) {
		return ParticipeDao.findById(id).get();
	}

}

	
	

