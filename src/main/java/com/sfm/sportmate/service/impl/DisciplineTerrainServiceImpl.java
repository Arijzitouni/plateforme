package com.sfm.sportmate.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.DisciplineTerrainDao;

import com.sfm.sportmate.model.DisciplineTerrain;

import com.sfm.sportmate.service.DisciplineTerrainService;

	
	@Service(value = "DisciplineTerrain")
public class DisciplineTerrainServiceImpl implements DisciplineTerrainService {

		
		@Autowired
		private DisciplineTerrainDao DisciplineTerrainDao;
		@Override
		public DisciplineTerrain save(DisciplineTerrain DisciplineTerrain) {
			return DisciplineTerrainDao.save(DisciplineTerrain);}
		
		@Override
		public List<DisciplineTerrain> findAll() {
			List<DisciplineTerrain> list = new ArrayList<>();
			DisciplineTerrainDao.findAll().iterator().forEachRemaining(list::add);
			return list;
		}
		
		@Override
		public void delete(long id) {
			DisciplineTerrainDao.deleteById(id);
		}
		
		@Override
		public DisciplineTerrain findById(Long id) {
			return DisciplineTerrainDao.findById(id).get();
		}
}

		

