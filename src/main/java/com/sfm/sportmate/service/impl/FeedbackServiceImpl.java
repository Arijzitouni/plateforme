package com.sfm.sportmate.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.FeedbackDao;

import com.sfm.sportmate.model.Feedback;

import com.sfm.sportmate.service.FeedbackService;



@Service(value = "Feedback")
public class FeedbackServiceImpl implements FeedbackService {

	
	@Autowired
	private FeedbackDao FeedbackDao;
	@Override
	public Feedback save(Feedback Feedback) {
		return FeedbackDao.save(Feedback);}
	
	@Override
	public List<Feedback> findAll() {
		List<Feedback> list = new ArrayList<>();
		FeedbackDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}
	
	@Override
	public void delete(long id) {
		FeedbackDao.deleteById(id);
	}
	
	@Override
	public Feedback findById(Long id) {
		return FeedbackDao.findById(id).get();
	}
}
	
	
	
	

