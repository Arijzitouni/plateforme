package com.sfm.sportmate.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.PhotosDao;

import com.sfm.sportmate.model.Photos;

import com.sfm.sportmate.service.PhotosService;


@Service(value = "Photos")
public class PhotosServiceImpl implements PhotosService  {

	@Autowired
	private PhotosDao PhotosDao;
	@Override
	public Photos save(Photos Photos) {
		return PhotosDao.save(Photos);}
	
	@Override
	public List<Photos> findAll() {
		List<Photos> list = new ArrayList<>();
		PhotosDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}
	
	@Override
	public void delete(long id) {
		PhotosDao.deleteById(id);
	}
	
	@Override
	public Photos findById(Long id) {
		return PhotosDao.findById(id).get();
	}
}

