package com.sfm.sportmate.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.TerrainDao;
import com.sfm.sportmate.model.Terrain;
import com.sfm.sportmate.service.TerrainService;

	
	@Service(value = "TerrainService")
	public class TerrainServiceImpl implements TerrainService {
	
		@Autowired
		private TerrainDao TerrainDao;
		@Override
		public Terrain save(Terrain Terrain) {
			return TerrainDao.save(Terrain);}
		
		@Override
		public List<Terrain> findAll() {
			List<Terrain> list = new ArrayList<>();
			TerrainDao.findAll().iterator().forEachRemaining(list::add);
			return list;
		}
		
		@Override
		public void delete(long id) {
			TerrainDao.deleteById(id);
		}
		
		@Override
		public Terrain findById(Long id) {
			return TerrainDao.findById(id).get();
		}
}
