package com.sfm.sportmate.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.UserDao;
import com.sfm.sportmate.dto.UserDto;
import com.sfm.sportmate.model.Role;
import com.sfm.sportmate.model.User;
import com.sfm.sportmate.service.UserService;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDao.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username or password");
		}
		if (!user.isEnable()) {
			throw new UsernameNotFoundException("User deactivated");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				getAuthority());
	}

	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("Administrateur"));
	}

	public List<User> findAll() {
		List<User> list = new ArrayList<>();
		userDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public List<User> findUserByRole(Role role) {
		List<User> list = new ArrayList<>();
		userDao.findUserByRole(role).iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		userDao.deleteById(id);
	}

	@Override
	public User findOne(String username) {
		return userDao.findByUsername(username);
	}

	@Override
	public User findById(Long id) {
		return userDao.findById(id).get();
	}

	@Override
	public User save(UserDto user) {
		User newUser = new User();

		newUser.setIdUser(user.getIdUser());
		newUser.setUsername(user.getUsername());
		
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		
		newUser.setRole(user.getRole());
		newUser.setNom(user.getNom());
		newUser.setPrenom(user.getPrenom());
		newUser.setTelephone(user.getTelephone());
		newUser.setEmail(user.getEmail());

		newUser.setEnable(user.isEnable());

		return userDao.save(newUser);
	}
	
}
