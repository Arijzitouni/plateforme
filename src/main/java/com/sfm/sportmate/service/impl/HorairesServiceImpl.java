package com.sfm.sportmate.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.HorairesDao;

import com.sfm.sportmate.model.Horaires;

import com.sfm.sportmate.service.HorairesService;


@Service(value = "HorairesService")
public class HorairesServiceImpl implements HorairesService{

	@Autowired
	private HorairesDao HorairesDao;
	@Override
	public Horaires save(Horaires Horaires) {
		return HorairesDao.save(Horaires);}
	
	@Override
	public List<Horaires> findAll() {
		List<Horaires> list = new ArrayList<>();
		HorairesDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}
	
	@Override
	public void delete(long id) {
		HorairesDao.deleteById(id);
	}
	
	@Override
	public Horaires findById(Long id) {
		return HorairesDao.findById(id).get();
	}
}
	

