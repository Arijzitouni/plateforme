
package com.sfm.sportmate.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.ItemsDao;

import com.sfm.sportmate.model.Items;

import com.sfm.sportmate.service.ItemsService;


@Service(value = "ItemsService")

public class ItemsServiceImpl implements ItemsService  {
	
	@Autowired
	private ItemsDao ItemsDao;
	@Override
	public Items save(Items Items) {
		return ItemsDao.save(Items);}
	
	@Override
	public List<Items> findAll() {
		List<Items> list = new ArrayList<>();
		ItemsDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}
	
	@Override
	public void delete(long id) {
		ItemsDao.deleteById(id);
	}
	
	@Override
	public Items findById(Long id) {
		return ItemsDao.findById(id).get();
	}
}