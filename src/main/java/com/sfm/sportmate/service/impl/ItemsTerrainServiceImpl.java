package com.sfm.sportmate.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.ItemsTerrainDAO;

import com.sfm.sportmate.model.ItemsTerrain;

import com.sfm.sportmate.service.ItemsTerrainService;


@Service(value = "ItemsTerrainService")

public class ItemsTerrainServiceImpl implements ItemsTerrainService  {
	
	@Autowired
	private ItemsTerrainDAO ItemsDAO;
	@Override
	public ItemsTerrain save(ItemsTerrain ItemsTerrain) {
		return ItemsDAO.save(ItemsTerrain);}
	
	@Override
	public List<ItemsTerrain> findAll() {
		List<ItemsTerrain> list = new ArrayList<>();
		ItemsDAO.findAll().iterator().forEachRemaining(list::add);
		return list;
	}
	
	@Override
	public void delete(long id) {
		ItemsDAO.deleteById(id);
	}
	
	@Override
	public ItemsTerrain findById(Long id) {
		return ItemsDAO.findById(id).get();
	}
}

	
	
	
	


