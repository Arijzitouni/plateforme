package com.sfm.sportmate.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.MatchDao;
import com.sfm.sportmate.model.Match;
import com.sfm.sportmate.service.MatchService;

	
	@Service(value = "MatchService")
	public class MatchServiceImpl implements MatchService {
	
		@Autowired
		private MatchDao MatchDao;
		@Override
		public Match save(Match Match) {
			return MatchDao.save(Match);}
		
		@Override
		public List<Match> findAll() {
			List<Match> list = new ArrayList<>();
			MatchDao.findAll().iterator().forEachRemaining(list::add);
			return list;
		}
		
		@Override
		public void delete(long idmatch) {
			MatchDao.deleteById(idmatch);
		}
		
		@Override
		public Match findById(Long idmatch) {
			return MatchDao.findById(idmatch).get();
		}
}
