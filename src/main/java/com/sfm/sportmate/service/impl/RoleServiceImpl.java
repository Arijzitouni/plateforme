package com.sfm.sportmate.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.RoleDao;
import com.sfm.sportmate.model.Role;
import com.sfm.sportmate.service.RoleService;


@Service(value = "roleService")
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleDao roleDao;

	@Override
	public Role save(Role role) {
		return roleDao.save(role);
	}

	@Override
	public List<Role> findAll() {
		List<Role> list = new ArrayList<>();
		roleDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		roleDao.deleteById(id);
		
	}

	@Override
	public Role findById(Long id) {
		return roleDao.findById(id).get();
	}

}
