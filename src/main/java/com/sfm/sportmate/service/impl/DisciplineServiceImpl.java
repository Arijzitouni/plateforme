
package com.sfm.sportmate.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sfm.sportmate.dao.DisciplineDao;
import com.sfm.sportmate.model.Discipline;
import com.sfm.sportmate.service.DisciplineService;

	
	@Service(value = "Discipline")
public class DisciplineServiceImpl implements DisciplineService {

		@Autowired
		private DisciplineDao DisciplineDao;
		@Override
		public Discipline save(Discipline Discipline) {
			return DisciplineDao.save(Discipline);}
		
		@Override
		public List<Discipline> findAll() {
			List<Discipline> list = new ArrayList<>();
			DisciplineDao.findAll().iterator().forEachRemaining(list::add);
			return list;
		}
		
		@Override
		public void delete(long id) {
			DisciplineDao.deleteById(id);
		}
		
		@Override
		public Discipline findById(Long id) {
			return DisciplineDao.findById(id).get();
		}
}
		

