package com.sfm.sportmate.service;
import java.util.List;

import com.sfm.sportmate.model.Photos;
public interface PhotosService {


	

	Photos save(Photos Photos);
		
	    List<Photos> findAll();
	    
	    void delete(long id);
	    
	    Photos findById(Long id);
	    
	}
