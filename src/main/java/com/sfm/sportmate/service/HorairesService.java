package com.sfm.sportmate.service;
import java.util.List;

import com.sfm.sportmate.model.Horaires;
public interface HorairesService {


	

	Horaires save(Horaires Horaires);
		
	    List<Horaires> findAll();
	    
	    void delete(long id);
	    
	    Horaires findById(Long id);
	    
	}
