package com.sfm.sportmate.service;

import java.util.List;

import com.sfm.sportmate.model.Role;

public interface RoleService {
	
	Role save(Role role);
	
    List<Role> findAll();
    
    void delete(long id);
    
    Role findById(Long id);
   
}
