package com.sfm.sportmate.service;

import java.util.List;

import com.sfm.sportmate.model.Terrain;

public interface TerrainService {

Terrain save(Terrain Terrain);
	
    List<Terrain> findAll();
    
    void delete(long id);
    
    Terrain findById(Long id);
    
}