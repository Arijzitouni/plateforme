package com.sfm.sportmate.service;

import java.util.List;

import com.sfm.sportmate.model.Discipline;

public interface DisciplineService {

	Discipline save(Discipline Discipline);
	
    List<Discipline> findAll();
    
    void delete(long id);
    
    Discipline findById(Long id);
    
}