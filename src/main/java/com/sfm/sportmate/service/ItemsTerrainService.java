package com.sfm.sportmate.service;
import java.util.List;

import com.sfm.sportmate.model.ItemsTerrain;
public interface ItemsTerrainService {


	

	ItemsTerrain save(ItemsTerrain ItemsTerrain);
		
	    List<ItemsTerrain> findAll();
	    
	    void delete(long id);
	    
	    ItemsTerrain findById(Long id);
	    
	}

