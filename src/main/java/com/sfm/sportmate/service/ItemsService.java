package com.sfm.sportmate.service;
import java.util.List;

import com.sfm.sportmate.model.Items;
public interface ItemsService {


	

	Items save(Items Items);
		
	    List<Items> findAll();
	    
	    void delete(long id);
	    
	    Items findById(Long id);
	    
	}
