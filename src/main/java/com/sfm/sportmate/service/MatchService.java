package com.sfm.sportmate.service;

import java.util.List;

import com.sfm.sportmate.model.Match;

public interface MatchService {

Match save(Match Match);
	
    List<Match> findAll();
    
    void delete(long id);
    
    Match findById(Long id);
    
}