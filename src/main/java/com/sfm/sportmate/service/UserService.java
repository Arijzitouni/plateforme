package com.sfm.sportmate.service;

import java.util.List;

import com.sfm.sportmate.dto.UserDto;
import com.sfm.sportmate.model.Role;
import com.sfm.sportmate.model.User;

public interface UserService {

    User save(UserDto user);
    List<User> findAll();
    void delete(long id);
    User findOne(String username);

    User findById(Long id);
    List<User> findUserByRole(Role role);
    
}
