package com.sfm.sportmate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.sportmate.model.Terrain;
import com.sfm.sportmate.service.TerrainService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/terrain")
public class TerrainController {

	@Autowired
	private TerrainService TerrainService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Terrain saveTerrain(@RequestBody Terrain Terrain) {
		return TerrainService.save(Terrain);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Terrain> listTerrain() {
		return TerrainService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Terrain getById(@RequestParam(name = "id") Long id) {
		return TerrainService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		TerrainService.delete(id);
	}
	
}
