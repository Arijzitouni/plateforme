package com.sfm.sportmate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.sportmate.model.Match;
import com.sfm.sportmate.service.MatchService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/match")
public class MatchController {

	@Autowired
	private MatchService MatchService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Match saveMatch(@RequestBody Match Match) {
		return MatchService.save(Match);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Match> listMatch() {
		return MatchService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Match getById(@RequestParam(name = "id") Long id) {
		return MatchService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		MatchService.delete(id);
	}
	
}

