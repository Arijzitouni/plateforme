package com.sfm.sportmate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.sportmate.model.Feedback;
import com.sfm.sportmate.service.FeedbackService;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/Feedback")


public class FeedbackController {

	@Autowired
	private FeedbackService FeedbackService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Feedback saveFeedback(@RequestBody Feedback Feedback) {
		return FeedbackService.save(Feedback);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Feedback> listFeedback() {
		return FeedbackService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Feedback getById(@RequestParam(name = "id") Long id) {
		return FeedbackService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		FeedbackService.delete(id);
	}
	
	
	
	
	
}
