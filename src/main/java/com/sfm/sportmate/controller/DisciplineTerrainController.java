package com.sfm.sportmate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.sportmate.model.DisciplineTerrain;
import com.sfm.sportmate.service.DisciplineTerrainService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/DisciplineTerrain")
public class DisciplineTerrainController {

	@Autowired
	private DisciplineTerrainService DisciplineTerrainService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public DisciplineTerrain saveDisciplineTerrain(@RequestBody DisciplineTerrain DisciplineTerrain) {
		return DisciplineTerrainService.save(DisciplineTerrain);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<DisciplineTerrain> listDisciplineTerrain() {
		return DisciplineTerrainService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public DisciplineTerrain getById(@RequestParam(name = "id") Long id) {
		return DisciplineTerrainService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		DisciplineTerrainService.delete(id);
	}
	
}


	

