package com.sfm.sportmate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.sfm.sportmate.config.JwtTokenUtil;
import com.sfm.sportmate.dao.UserDao;
import com.sfm.sportmate.dto.AuthToken;
import com.sfm.sportmate.dto.LoginUser;
import com.sfm.sportmate.dto.UserDto;
import com.sfm.sportmate.model.User;
import com.sfm.sportmate.service.UserService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private UserDao ut;
    
    @Autowired
    PasswordEncoder encoder;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> register(@RequestBody LoginUser loginUser) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginUser.getUsername(),
                        loginUser.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final User user = userService.findOne(loginUser.getUsername());
        if(user.isEnable()) {
        	final String token = jwtTokenUtil.generateToken(user);
            return ResponseEntity.ok(new AuthToken(token,user));
        } else 
        	return new ResponseEntity<>("User disabled", HttpStatus.UNAUTHORIZED);
        
    }
    
    @PostMapping(value="/Register")
	 public ResponseEntity<String> AddUser(@RequestBody UserDto signUpRequest){
	     /* if(cr.existsByUsername(signUpRequest.getUsername()) && ut.existsByUsername(signUpRequest.getUsername())) {
	            return new ResponseEntity<String>("Fail -> Username is already taken!",
	                    HttpStatus.BAD_REQUEST);
	        }
	        if(cr.existsByEmail(signUpRequest.getEmail())) {
	            return new ResponseEntity<String>("Fail -> Email is already in use!",
	                    HttpStatus.BAD_REQUEST);
	        }*/
	        // Creating user's account
	       User user = new User (signUpRequest.getUsername(),encoder.encode(signUpRequest.getPassword()),signUpRequest.getEmail(),signUpRequest.getIdUser(),signUpRequest.getNom(),signUpRequest.getPrenom(),signUpRequest.getRole(),signUpRequest.getTelephone(),signUpRequest.isEnable()); 
	       ut.save(user);
	       return ResponseEntity.ok().body("User registered successfully!");
    }
	
	

}
