package com.sfm.sportmate.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.sfm.sportmate.model.Photos;

import com.sfm.sportmate.service.PhotosService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/Photos")
public class PhotosController {
	
	
	@Autowired
	private PhotosService PhotosService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Photos savePhotos(@RequestBody Photos Photos) {
		return PhotosService.save(Photos);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Photos> listPhotos() {
		return PhotosService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Photos getById(@RequestParam(name = "id") Long id) {
		return PhotosService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		PhotosService.delete(id);
	}
	
}

	
	
	


