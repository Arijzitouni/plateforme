package com.sfm.sportmate.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.sportmate.model.Participe;

import com.sfm.sportmate.service.ParticipeService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/Participe")
public class ParticipeController {

	
	@Autowired
	private ParticipeService ParticipeService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Participe saveParticipe(@RequestBody Participe Participe) {
		return ParticipeService.save(Participe);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Participe> listParticipe() {
		return ParticipeService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Participe getById(@RequestParam(name = "id") Long id) {
		return ParticipeService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		ParticipeService.delete(id);
	}

}

	
	
	
	

