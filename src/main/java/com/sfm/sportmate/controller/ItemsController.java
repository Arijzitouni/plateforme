package com.sfm.sportmate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.sportmate.model.Items;
import com.sfm.sportmate.service.ItemsService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/Items")
public class ItemsController {

	@Autowired
	private ItemsService ItemsService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Items saveItemsTerrain(@RequestBody Items Items) {
		return ItemsService.save(Items);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Items> listItems() {
		return ItemsService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Items getById(@RequestParam(name = "id") Long id) {
		return ItemsService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		ItemsService.delete(id);
	}
	
}
