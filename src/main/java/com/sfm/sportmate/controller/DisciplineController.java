package com.sfm.sportmate.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.sportmate.model.Discipline;
import com.sfm.sportmate.service.DisciplineService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/Discipline")
public class DisciplineController {
	
	@Autowired
	private DisciplineService DisciplineService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Discipline saveDiscipline(@RequestBody Discipline Discipline) {
		return DisciplineService.save(Discipline);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Discipline> listDiscipline() {
		return DisciplineService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Discipline getById(@RequestParam(name = "id") Long id) {
		return DisciplineService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		DisciplineService.delete(id);
	}
	
}


