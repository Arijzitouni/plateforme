package com.sfm.sportmate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sfm.sportmate.model.Horaires;

import com.sfm.sportmate.service.HorairesService;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/Horaires")

public class HorairesController {
	
	@Autowired
	private HorairesService HorairesService;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public Horaires saveHoraires  (@RequestBody Horaires Horaires) {
		return HorairesService.save(Horaires);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public List<Horaires> listHoraires() {
		return HorairesService.findAll();
		
	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Horaires getById(@RequestParam(name = "id") Long id) {
		return HorairesService.findById(id);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteById(@RequestParam(name = "id") Long id) {
		HorairesService.delete(id);
	}
	
}

	
	


