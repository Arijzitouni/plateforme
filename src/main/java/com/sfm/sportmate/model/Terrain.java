package com.sfm.sportmate.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;




@Entity
public class Terrain {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column
	private String nom;
	@Column
	private String adresse;
	@Column
	private Float longitude;
	@Column
	private Float latitude;
	@Column
	private String responsable;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public Float getLongitude() {
		return (longitude);
	}

	public void setLongitude(Float Longitude) {
		this.longitude = Longitude;
	}
	public Float getLatitude() {
		return (latitude);
	}

	public void setLatitude(Float Latitude) {
		this.latitude = Latitude;
	}
	
	@Override
	public String toString() {
		return "UserDto [id=" + id + ", adresse=" + adresse + ", responsable=" + responsable + ",, nom="
				+ nom + " ]";
	}

}
	
	
	
	
	
	


