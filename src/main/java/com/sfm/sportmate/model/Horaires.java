package com.sfm.sportmate.model;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Horaires {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	
	@Column
	public Date Datedeb;
	@Column
	public Date Datefin;
	
	@Column
	public long getId() {
		return id;
	}

	public void setIditem(long id) {
		this.id = id;
	}
	public Date getDatedeb() {
		return Datedeb;
	}

	public void setDatedeb(Date Datedeb) {
		this.Datedeb = Datedeb;
	}
	
	public Date getDatefin() {
		return Datefin;
	}

	public void setDatefin(Date Datefin) {
		this.Datefin = Datefin;
	}
	
	
	@Override
	public String toString() {
		return "Horaires [id=" + id + ",Datedeb=" + Datedeb + ",Datefin=" + Datefin + "]";
	}
	
}
