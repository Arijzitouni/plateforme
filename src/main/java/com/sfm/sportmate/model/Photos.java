package com.sfm.sportmate.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Photos {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	@Column
	public String url;

	public String getUrl() {
		return url;
	}

	public void seturl(String url) {
		this.url = url;
	}
	
	@Override
	public String toString() {
		return "Photos [url=" + url + "]";
	}
	
}
