package com.sfm.sportmate.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idUser;

	@Column(unique = true)
	private String username;
	@Column
	@JsonIgnore
	private String password;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role")
	private Role role;

	@Column
	private String nom;
	@Column
	private String prenom;
	@Column
	private String email;
	@Column
	private String telephone;

	@Column
	private boolean enable;
	
	

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(String username, String password, String email, Long idUser,String nom, String prenom, Role role,String telephone, Boolean enable) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.idUser=idUser;
		this.nom=nom;
		this.prenom=prenom;
		this.role=role;
		this.telephone=telephone;
		this.enable=enable;
		
	}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", username=" + username + ", password=" + password + ", role=" + role + ", nom="
				+ nom + ", prenom=" + prenom + ", email=" + email + ", telephone=" + telephone + ", enable=" + enable
				+ "]";
	}

}
