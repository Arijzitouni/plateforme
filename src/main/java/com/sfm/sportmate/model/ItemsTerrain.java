package com.sfm.sportmate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ItemsTerrain {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long iditem;
	
	@Column
	public String etat;
	@Column
	public long getIditem() {
		return iditem;
	}

	public void setIditem(long iditem) {
		this.iditem = iditem;
	}
	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}
	
	@Override
	public String toString() {
		return "ItemTerrain [iditem=" + iditem + ",etat=" + etat + "]";
	}

	
}
