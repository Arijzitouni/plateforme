package com.sfm.sportmate.model;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
@Entity
@Table(name="value_match")
public class Match {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idmatch;
	
	@Column
	private String initiateur;
	@Column
	private String participants;
	@Column
	private Float nbjoueurs;
	@Column
	private Date datej;
	
	public long getId() {
		return idmatch;
	}
	public void setId(long idmatch) {
		this.idmatch = idmatch;
	}
	
	public String getInitiateur() {
		return initiateur;
	}

	public void setinitiateur(String initiateur) {
		this.initiateur = initiateur;
	}
	public String getParticipants() {
		return participants;
	}

	public void setParticipants(String participants) {
		this.participants = participants;
	}
	public Date getDate() {
		return datej;
	}

	public void setDate (Date datej) {
		this.datej = datej;
	}
	public Float getNbjoueurs() {
		return nbjoueurs;
	}

	public void setNbjoueurs(Float nbjoueurs) {
		this.nbjoueurs = nbjoueurs;
	}

	@Override
	public String toString() {
		return "MatchDto [idmatch=" + idmatch + ", initiateur=" + initiateur + ",nbjoueurs=" +nbjoueurs+", participants=" + participants + ",, datej="
				+ datej + " ]";
	}}
	
	

