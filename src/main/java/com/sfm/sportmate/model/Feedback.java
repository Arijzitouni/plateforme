package com.sfm.sportmate.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Feedback {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long id;
	@Column
	public String categorie;
	@Column
	public String description;
	
	@Column
	public long getId() {
		return id;
	}
	public String getCategorie() {
		return categorie;
	}

	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}

	public String getdescription() {
		return description;
	}

	public void setDesc(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Feedbackk [id=" + id + ",categorie=" + categorie + ",description=" + description + "]"  ;
}
}