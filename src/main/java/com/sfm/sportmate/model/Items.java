package com.sfm.sportmate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Items {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long idj;
	@Column
	public String libelle;
	
	@Column
	public long getIdj() {
		return idj;
	}

	public void setIditem(long idj) {
		this.idj = idj;
	}
	
	public String getLibelle() {
		return libelle;
	}

	public void setEtat(String libelle) {
		this.libelle = libelle;
	}
	@Override
	public String toString() {
		return "Items [idj=" + idj + ",libelle=" + libelle + "]";
	}
	
	
}
