package com.sfm.sportmate.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Participe {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	private long idUser;
	@Column
	private long idmatch;
	@Column
	private String QRcode;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	public String getQRcode() {
		return QRcode;
	}

	public void setQRcode(String QRcode) {
		this.QRcode = QRcode;
	}
	public long getidUser() {
		return idUser;
	}

	public void setidUser(long idUser) {
		this.idUser = idUser;
	}
	public long getidmatch() {
		return idmatch;
	}

	public void setidmatch(long idmatch) {
		this.idmatch = idmatch;
	}
	@Override
	public String toString() {
		return "Participe [id=" + id + ", QRcode=" + QRcode + ",idUser=" + idUser + ", idmatch=" + idmatch + "]";
	}
}
